<?php
/**
 * @copyright 2013
 * @author Progress44 <dev@progress44.com>
 * @version 1.0 ClubsMade API SDK
 * @todo n/a
 * @access public
 */
 
require_once(dirname(__FILE__) . "/unirest/lib/Unirest.php");

/**
 * @package Public\
 * @since 1.0 this was introduced on version 1.0
 * @uses Unirest
 */
class ClubsMade
{
    /**
	 * @property stdObject $instance the instance of the current class
	 * @api
	 * @static
	 */
    private static $instance;
    
    /**
	 * @property string $apiPrivate the private API key
	 * @api
	 * @static
	 */
    private static $apiPrivate = "";

    /**
	 * @property string $apiPublic the public API key
	 * @api
	 * @static
	 */
    private static $apiPublic = "";

    /**
	 * @property String[] array $servers contains the servers
	 * @api
	 * @static
	 */
    private static $servers = array("api","demo","my","cdn");

    /**
	 * @property String[] array $protocols list of the protocols allowed by the server
	 * @api
	 * @static
	 */
    private static $protocols = array("http://","https://");

    /**
	 * @property String[] array $methods list of the http methods allowed by the server
	 * @api
	 * @static
	 */
    private static $methods = array("PUT","POST","GET","DELETE");

    /**
	 * @property String[] array $outputs list of the output formats
	 * @api
	 * @static
	 */
    private static $outputs = array("xml","json","html");

    /**
	 * @constant constant CM_DMN contains the primary clubsmade domain
	 * @api
	 * @constant
	 */
    const CM_DMN = "clubsmade.com";

    /**
	 * @constant constant CM_API_PATH contains the path to the public API
	 * @api
	 * @constant
	 */
    const CM_API_PATH = "/public";
    
    /**
	 * @constant constant CM_API_VERSION contains the version of the public API
	 * @api
	 * @constant
	 */
    const CM_API_VERSION = "/v1";
    
    
    /**
	 * @constant constant CM_SERVER_API corresponds to server list API
	 * @api
	 * @constant
	 */
    const CM_SERVER_API = 0;

    /**
	 * @constant constant CM_SERVER_DEMO corresponds to server list DEMO
	 * @api
	 * @constant
	 */
    const CM_SERVER_DEMO = 1;

    /**
	 * @constant constant CM_SERVER_MY corresponds to server list MY
	 * @api
	 * @constant
	 */
    const CM_SERVER_MY = 2;

    /**
	 * @constant constant CM_SERVER_CDN corresponds to server list CDN
	 * @api
	 * @constant
	 */
    const CM_SERVER_CDN = 3;


    /**
	 * @constant constant CM_PROTOCOL_OPEN corresponds to protocol list http
	 * @api
	 * @constant
	 */
    const CM_PROTOCOL_OPEN = 0;

    /**
	 * @constant constant CM_PROTOCOL_SECURE corresponds to protocol list https
	 * @api
	 * @constant
	 */
    const CM_PROTOCOL_SECURE = 1;


    /**
	 * @constant constant CM_METHOD_PUT corresponds to method list PUT
	 * @api
	 * @constant
	 */
    const CM_METHOD_PUT = 0;

    /**
	 * @constant constant CM_METHOD_POST corresponds to method list POST
	 * @api
	 * @constant
	 */
    const CM_METHOD_POST = 1;

    /**
	 * @constant constant CM_METHOD_GET corresponds to method list GET
	 * @api
	 * @constant
	 */
    const CM_METHOD_GET = 2;

    /**
	 * @constant constant CM_METHOD_DELETE corresponds to method list DELETE
	 * @api
	 * @constant
	 */
    const CM_METHOD_DELETE = 3;


    /**
	 * @constant constant CM_OUTPUT_XML corresponds to output list XML
	 * @api
	 * @constant
	 */
    const CM_OUTPUT_XML = 0;

    /**
	 * @constant constant CM_OUTPUT_JSON corresponds to output list JSON
	 * @api
	 * @constant
	 */
    const CM_OUTPUT_JSON = 1;

    /**
	 * @constant constant CM_OUTPUT_HTML corresponds to output list HTML
	 * @api
	 * @constant
	 */
    const CM_OUTPUT_HTML = 2;

    /**
	 * @property String $url will be populated with the final URL (without object) by Construct
	 */
    private $url;

    /**
	 * @property String[] array $callKeys needed by the _call magic function name processing
	 */
    private $callKeys = array();


    /**
	 * @version 1.0
	 * @copyright inherit
	 * @param null
	 * @return ClubsMade $this returns the instance of this class
	 * @uses n/a
	 * @throws n/a
	 * @since 1.0
	 */
	 public function __construct(
	 	$apiPrivate,
	 	$apiPublic, 
	 	$protocol = self::CM_PROTOCOL_OPEN, 
	 	$server = self::CM_SERVER_API, 
	 	$format = self::CM_OUTPUT_JSON,
	 	$path = self::CM_API_PATH, 
	 	$version = self::CM_API_VERSION)
	 {
	 	self::$apiPrivate = $apiPrivate;
	 	self::$apiPublic  = $apiPublic;

	 	$url  = "";
	 	$url .= self::$protocols[$protocol];
	 	$url .= self::$servers[$server];
	 	$url .= ".".self::CM_DMN;
	 	$url .= $path;
	 	$url .= $version;
	 	$url .= "/%s";
	 	$url .= "%s";
	 	$url .= ".".self::$outputs[$format];

	 	$this->url = $url;

	 	return $this;
	 }

	 public function __call($name, $arguments)
	 {
	 	$a = $this->splitKeywords($name);
	 	$c = count($a);
	 	$d = is_array(@$arguments[0]) ? $arguments[0] : array();

	 	if($c == 1)
	 		return; 

	 	$object = strtolower($a[$c-1]);
	 	$action = strtoupper(implode("", array_splice($a, 0, $c-1)));

	 	return $this->call($object, $action, $d);
	 }

	 /**
	 * @version 1.0
	 * @copyright inherit
	 * @return 
	 * @uses n/a
	 * @throws n/a
	 * @since 1.0
	 */
	 public function call($object,$action,$data = array())
	 {
	 	$aeqm = true;

	 	if(in_array($action, self::$methods))
	 		$method = $action;
	 	else if(is_numeric($action))
	 		$method = self::$methods[$action];
	 	else
	    {
	 		$method = self::$methods[self::CM_METHOD_GET];
	 		$aeqm 	= false;
	 	}

	 	$get 		= ($method == self::$methods[self::CM_METHOD_GET] ? "/".strtolower(self::$methods[self::CM_METHOD_GET]) : "");

	 	if($method != self::$methods[self::CM_METHOD_PUT] || $method != self::$methods[self::CM_METHOD_POST])
	 		$headers = array(
	 			"Accept" => "*/*", 
	 			"Content-Type" => "application/x-www-form-urlencoded", 
	 			"Expect" => "100-continue",
	 			"User-Agent" => "ClubsMadeSDK"
	 		);
	 	else
	 		$headers = array(
	 			"Accept" => "*/*", 
	 			"Expect" => "100-continue", 
	 			"User-Agent" => "ClubsMadeSDK"
	 		);

	 	$method 	= strtolower($method);
	 	//$url 		= $aeqm ? sprintf($this->url, $object, $get) : sprintf($this->url, $object, "/".strtolower($action));
	 	$url 		= sprintf($this->url, $object, "/".$action);
	 	//$url 	   .= "?api=".@self::$apiPrivate."&m=1";

	 	$data 		= array_merge(
	 		array(
		 		"api" => @self::$apiPrivate,
				"m"  => 1
	 		),
	 		$data
	 	);

	 	
	 	$d = array();
	 	foreach($data as $key=>$value)
	 		$d[] = $key."=".$value;

	 	if($method != self::$methods[self::CM_METHOD_GET])
	 	 	 $data = implode("&", $d);
		

	 	//$response 	= Unirest::{$method}($url, $headers, $data);
	 	$response 	= Unirest::get($url."?".$data, $headers, $data);
    	
    	return (object) array(
     		"headers" 	=> $response->headers, 
     		"body" 		=> $response->body, 
     		"code" 		=> $response->code,
     		"raw"		=> $response->raw_body
     	);
	 }

	 public function testCall($object,$action,$data = array())
	 {
	 	return $this->call($object,$action,$data);
	 }

	/**
	 * @version 1.0
	 * @copyright inherit
	 * @return mixed[] self::$splittingKeys array containing the call method name splitted
	 * @param string $string the method call name
	 * @uses n/a
	 * @throws n/a
	 * @api
	 * @since 1.0
	 */
     public function splitKeywords($string)
     {
     	$match = array();
     	preg_match_all("/([A-Z][^.A-Z]*)/", $string, $match);

     	$nm = array($string);
     	foreach($match[0] as $k)
     		$nm[0] = str_replace($k, "", $nm[0]);

     	$nm = array_merge($nm,$match[0]);

     	return $nm;
     }

	/**
	 * @version 1.0
	 * @copyright inherit
	 * @param String $e error message to keep track of
	 * @return Exception_Exception_XException $this
	 * @uses $this->changeStatusToERROR() changes the status to error
	 * @throws n/a
	 * @since 1.0
	 */
	 public function __get($name)
	 {
	 	if($name == "properties")
	 		return ($this->properties);
		return null;
	 }

	 /**
	 * @version 1.0
	 * @copyright inherit
	 * @param String $e error message to keep track of
	 * @return Exception_Exception_XException $this
	 * @uses $this->changeStatusToERROR() changes the status to error
	 * @throws n/a
	 * @since 1.0
	 */
	 public function __set($name,$value)
	 {	
	 	return $value;
	 }

}// [c]

?>