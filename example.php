<?php
require_once(dirname(__FILE__) . "/clubsmade.api.php");

$apiPrivate = "";
$apiPublic  = "";

$cm = new ClubsMade($apiPrivate, $apiPublic);

//echo json_encode($cm->testCall("club","GET",array("id" => 19)));
/**/
$u = $cm->getClub(
	array(
        "lat"       => 45.494,      
        "long"      => 12.5902,     
        "radius"    => 100,        // optional - default: 100
        "offset"    => 0,          // optional
        "limit"     => 2          // optional (required if offset is set)
    )
);

echo json_encode($u->body);
/**/
/*
$r = $cm->putUser(
    array(
        "firstname"  => "fn",        // optional
        "lastname"   => "ln",         // optional
        "fullname"   => "fn ln",         // optional
        "company"    => "comp",          // optional
        "department" => "dept",       // optional
        "note"       => "notes",             // optional
        "email"      => "fn1@ln.com",            
        "phone"      => "004339348132",            // suggested
        "username"   => "fnlncomp1",
        "password"   => "fnlncomp",
        "usergroup"  => 1,                  // 1
    )
);

//echo json_encode($r->body);
echo $r->raw;
/**/

exit();

?>