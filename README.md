# ClubsMade SDK [![Build Status](https://api.travis-ci.org/Mashape/unirest-php.png)](http://clubsmade.com)

L’SDK fornito è composto da diversi file. Questo pacchetto deve essere caricato sul server di chi vuole usufruire dei servizi di ClubsMade. 
Una volta caricato sul server, si deve includere il file principale (clubsmade.api.php) sulla vostra applicazione web. Questo file include tutti i file necessari del SDK.

### Install
ClubsMade-PHP requires PHP `v5.3+`. Download the PHP library from Github, and require in your script like so:

To include it in your scripts:

```
require_once '/path/to/clubsmade.sdk/clubsmade.api.php';
```

## Richiesta usando Unirest

Per fare richieste al server potete usare UniRest direttamente. Per la documentazione di Unirest aprite README.md che si trova dentro la cartella 'unirest'


## Server e API
L’API pubblica di ClubsMade si trova su:

	 Test: 		    api.clubsmade.com/public/v1/
	 Demo: 		    demo.clubsmade.com/public/v1/
	 Production: 	my.clubsmade.com/public/v1/

Tutte le chiamate devono essere identificate tramite la chiave API privata fornita da **ClubsMade**. 

Il server sopporta i seguenti metodi di chiamate: 
**Put**, **Get**, **Post** e **Delete**


# Request
La chiamata all’API è costituita dalle seguenti parti:

- `Protocollo` http:   http:// (in futuro sarà supportato anche https://)
- `Server`: 			api | demo | my
- `url server`: 		clubsmade.com
- `path dell’API`: 		/public
- `versione dell’API`: 	/v1
- `oggetto`: 			event | club | ticket | user
- `formato della risposta`: .json
- `parametri get se la chiamata si fa usando il metodo GET`: ?api=0az&… 

###Esempio di una chiamata all’API

- Dal browser: 
	- http://api.clubsmade.com/public/v1/event.json ***che equivale a***
	- http://api.clubsmade.com/public/v1/event/get.json 

Questa chiamata ha bisogno dei seguenti paramteri:

- api=000az la chiave API privata fornita da ClubsMade
- [Opzinale] id=00 l’id dell’evento che si vuole ricevere
- [Opzionale] club=00 l’id del club per avere tutti gli eventi di questo club. Serve per forza una delle due, id, club.
- m=1 questo parametro fa in modo che gli errori vengano restituiti nel formato JSON.

La chiamata finale diventa

[http://api.clubsmade.com/public/v1/event/get.json?api=xxx&m=1&club=19](http://api.clubsmade.com/public/v1/event/get.json?api=xxx&m=1&club=19)

# Response

La risposta è formata dalle seguenti

- `time`: contiene il tempo quando la risposta è stata fornita.
	- `timestamp`: tempo in millisecondi dal 1970
	- `formatted`: tempo formattato
- `code`: codice http della risposta (in caso di successo è 200)
- `status`: contiene una stringa, **ok** se la chiamata non presenta errori, **error** altrimenti
- `error`: **null** nel caso non ci siano stati errori, messaggio di errore altrimenti
- `result`: contiene la risposta effettiva dal server. Questa parte cambia e dipende dalla chiamata



#### Questa è una risposta formattata JSON nel caso di successo

```json
{
    "time": {
        "response": {
            "timestamp": 1412100989.7162,
            "formatted": "Tue, 30 Sep 2014 19:16:29 +0100"
        }
    },
    "code": 200,
    "result": {},
    "status": "ok",
    "error": null
}
```
#### Risposta in caso di errore

```json
{
    "code": 400,
    "result": false,
    "status": "error",
    "error": "Please provide either the id of the event or the id of the club"
}
```
## - SDK

### Installazione
ClubsMade-PHP requires PHP `v5.3+`. Download the PHP library from Github, and require in your script like so:

*Tutte le chiamate devono essere identificate tramite la chiave API privata fornita da ClubsMade.*

To include it in your scripts:

```
require_once '/path/to/clubsmade.sdk/clubsmade.api.php';
```

### Prova

Per provare se avete installato bene l'sdk eseguite il file `example.php` che si trova dentro l'sdk oppure eseguite il codice quì sotto.

```
require_once(dirname(__FILE__) . "/clubsmade.api.php");

$apiPrivate = "";
$apiPublic  = "";

$cm = new ClubsMade($apiPrivate, $apiPublic);

echo json_encode($cm->testCall("club","GET",array("id" => 19)));

exit();
```

### ClubsMade API Constants

*CM_DMN* contains the **primary clubsmade domain**
    
    CM_DMN = "clubsmade.com";

*CM_API_PATH* contains the path to the public **API**
	 
	CM_API_PATH = "/public";
    
*CM_API_VERSION* contains the version of the public **API**
	 
	CM_API_VERSION = "/v1";
    
*CM_SERVER_API* corresponds to server list **API**
	
	CM_SERVER_API = 0;

*CM_SERVER_DEMO* corresponds to server list **DEMO**
	 
	 CM_SERVER_DEMO = 1;

*CM_SERVER_MY* corresponds to server list **MY**
	
	CM_SERVER_MY = 2;

*CM_SERVER_CDN* corresponds to server list **CDN**
	
	CM_SERVER_CDN = 3;

*CM_PROTOCOL_OPEN* corresponds to protocol list **http**
	
	CM_PROTOCOL_OPEN = 0;

*CM_PROTOCOL_SECURE* corresponds to protocol list **https**

	CM_PROTOCOL_SECURE = 1;

*CM_METHOD_PUT* corresponds to method list **PUT**
	
	 CM_METHOD_PUT = 0;

*CM_METHOD_POST* corresponds to method list **POST**
	
	CM_METHOD_POST = 1;

*CM_METHOD_GET* corresponds to method list **GET**
	
	CM_METHOD_GET = 2;

*CM_METHOD_DELETE* corresponds to method list **DELETE**
	
	CM_METHOD_DELETE = 3;

*CM_OUTPUT_XML* corresponds to output list **XML**
	
	CM_OUTPUT_XML = 0;

*CM_OUTPUT_JSON* corresponds to output list **JSON**
	
	CM_OUTPUT_JSON = 1;

*CM_OUTPUT_HTML* corresponds to output list **HTML**
	
	CM_OUTPUT_HTML = 2;

### Inizializzare l'sdk
Per inizializzare l'oggetto del SDK usa il costruttore `ClubsMade` come segue:

```
$cm = new ClubsMade(
	$apiPrivate,
	$apiPublic, 
	$protocol = ClubsMade::CM_PROTOCOL_OPEN, 
	$server = ClubsMade::CM_SERVER_API, 
	$format = ClubsMade::CM_OUTPUT_JSON,
	$path = ClubsMade::CM_API_PATH, 
	$version = ClubsMade::CM_API_VERSION	
);
```

Gli argomenti `$protocol`,`$server`,`$format`,`$path` and `$version` non sono obbligatori. Quindi l'SDK si può inizializzare così:

```
$cm = new ClubsMade(
	$apiPrivate,
	$apiPublic
);
```

### Chiamare il server usando l'SDK

L'SDK è stata creata per facilitare le chiamate al server ClubsMade. Prima di dire come fare le chiamate al server bisogna descrivere le quatro entità accessibili dall'API pubblica. 

#### Entita pubbliche

- User
- Club
- Ticket
- Event

#### Azioni

Su ogni entità possono essere applicate delle azioni.
Di solito le azioni più comuni sono quelle che corrispondono ai metodi HTTP supportati dal server ovvero **PUT**, **GET**, **POST**, **DELETE** però non tutte queste sono abilitate per l'API pubblica. Allo stesso tempo ci sono altre azioni che non corrispondono a nessun metodo HTTP.

Di seguito le azioni ragruppate per ogni entità:

##### User

- `get`
	- **id** - integer, l'id dell'utente del quale si vogliono avere i dati
- `authenticate`
	- **username** - string, la username del utente da autenticare
	- **password** - string, la password del utente da autenticare
- `put`
	- ~~firstname~~ - string, **optional**, nome del nuovo utente
	- ~~lastname~~ - string, **optional**, cognome del nuovo utente
	- ~~fullname~~ - string, **optional**, nome e cognome
	- ~~company~~ - string, **optional**, nome dell'azienda dove l'utente lavora o se l'utente è un azienda il suo nome.
	- ~~department~~ - string, **optional**, dipartimento dove l'utente lavora, es. Amministrazione
	- ~~note~~ - string, **optional**, note varie sull'utente
	- **email**, string, l'email dell'utente
	- *phone*, string, **optional**, numero di telefono dell'utente. paramtero opzionale ma si suggerisce di fornirlo.
	- **username** - string, la username del nuovo utente. Si può usare anche l'email
	- **password** - string, la password del nuovo utente. Deve essere lunga almeno 8 charatteri.
	- **usergroup** - integer, il gruppo di appartenenza dell'utente. *1 per un utente nuovo*
	- ~~ccob~~ - string, **optional**, JSON, questo oggetto viene fornito compilando il form su `{SERVER}.clubsmade.com/payment/ccui.html` che può essere incluso sul sito con un iframe. Una volta completato, la pagina salva il risultato sul `localstorage` del HTML5 con il nome `cc`, e sull'input con `id="result"` e `name="ccob"`. 
	Un esempio del risultato può essere il seguente
	
```json
{
    "number": "•••• •••• •••• 1111",
    "exp_month": "11",
    "exp_year": "2019",
    "cvc": "•••",
    "amount_int": "15",
    "currency": "EUR",
    "cardholder": "Ani Sinanaj",
    "token": "tok_0701ef3e27f893d591c9",
    "bin": "411111",
    "binCountry": "DE",
    "brand": "VISA"
}
```

##### Club

- `get`
	- **id** - integer, l'id dell'club del quale si vogliono avere i dati. *Si può ommettere se si usano i parametri successivi.*
	- **lat** - integer, latitudine dove prendere i club più vicini.
	- **long** - integer, longitudine dove prendere i club più vicini.
	- radius - integer, **optional**, raggio del area dove prendere i club. Se non si specifica si usa **100** come raggio. (espresso in **km**)
	- offset - integer, **optional**, inpaginazione. Da dove iniziare ad avere i risultati. **se si utilizza, il parametro limit diventa obligatorio**
	- limit - integer, **optional**, inpaginazione. Quanti risultati mostrare.
- `covers`
	- **club** - integer, l'id del club del quale avere le cover foto
	- offset - integer, **optional**, inpaginazione. Da dove iniziare ad avere i risultati. **se si utilizza, il parametro limit diventa obligatorio**
	- limit - integer, **optional**, inpaginazione. Quanti risultati mostrare.
- `photos`
	- **club** - integer, l'id del club del quale avere le cover foto
	- offset - integer, **optional**, inpaginazione. Da dove iniziare ad avere i risultati. **se si utilizza, il parametro limit diventa obligatorio**
	- limit - integer, **optional**, inpaginazione. Quanti risultati mostrare.

##### Event

- `get`
	- **id** - integer, l'id del evento del quale si vogliono avere i dati. *Si può ommettere se si usano i parametri successivi.*
	- **club** - integer, l'id del club del quale si vogliono avere tutti gli eventi. *Si può ommettere se si usano i parametri successivi.*
	- **lat** - integer, latitudine dove prendere i club più vicini.
	- **long** - integer, longitudine dove prendere i club più vicini.
	- radius - integer, **optional**, raggio del area dove prendere i club. Se non si specifica si usa **100** come raggio. (espresso in **km**)
	- cancelled - integer, **optional**, se si passa con il valore 1, la chiamata restituisce anche eventi cancellati. *Valore di default è 0.*
	- offset - integer, **optional**, inpaginazione. Da dove iniziare ad avere i risultati. **se si utilizza, il parametro limit diventa obligatorio**
	- limit - integer, **optional**, inpaginazione. Quanti risultati mostrare.
- `covers`
	- **event** - integer, l'id del evento del quale avere le cover foto
	- offset - integer, **optional**, inpaginazione. Da dove iniziare ad avere i risultati. **se si utilizza, il parametro limit diventa obligatorio**
	- limit - integer, **optional**, inpaginazione. Quanti risultati mostrare.
- `photos`
	- **event** - integer, l'id del evento del quale avere le cover foto
	- offset - integer, **optional**, inpaginazione. Da dove iniziare ad avere i risultati. **se si utilizza, il parametro limit diventa obligatorio**
	- limit - integer, **optional**, inpaginazione. Quanti risultati mostrare.
- `contacts`
	- **event** - integer, l'id del evento del quale avere i contatti
	
##### Ticket

- `get`
	- **id** - integer, l'id del ticket del quale si vogliono avere i dati. *Si può ommettere se si usano i parametri successivi.*
	- **event** - integer, l'id del evento del quale si vogliono avere tutti i ticket.
- `book`
	- **userid** - integer, l'id dell'utente del quale avere le cover foto
	- **ticket** - string, lista degli id dei ticket selezionati dall'utente separati da virgola.
- `order`
	- **user** - integer, l'id dell'utente del quale avere tutti gli ordini. *Si può ommettere se si usano i parametri successivi.*
	- bought -integer, **optional**, se è 1 il risultato contiene tutti gli ordini comprati dall'utente `user`, se è 0 (valore di default), il risultato contiene gli ordini non comprati. (solo in combinazione con `user`)
	- **id** - integer, l'id del ordine del quale avere i dati. 
	
	
#### Fare le chiamate dall'SDK

Fare le chiamate al server usando l'SDK una volta che si hanno presenti le *Azioni* e le *Entità* diventa molto facile.

Infatti basta unire l'azione all'entità e si ha il metodo da chiamare all'SDK. Per avere le idee piu chiare vedi l'esempio quì sotto:

```
// inizializzazione dell'SDK come abbiamo visto sopra
$cm = new ClubsMade(
	$apiPrivate,
	$apiPublic
);

// per fare una chiamata a questo punto, si chiama il metodo 
// con il nome uguale all'azione da chiamare unito all'entità
// per esempio, per chiamare l'azione 'get' dell'entità 'user'
// usiamo:

$cm->getUser();
```

##### Passare gli argomenti/parametri all'SDK
L'esempio sopra manca i parametri. I parametri si passano nella forma di un Array Associativo es. `array("a" => 1);` dove la **stringa** che identifica il valore è il *nome del parametro*.

Quindi il codice sopra diventa:

```
// inizializzazione dell'SDK come abbiamo visto sopra
$cm = new ClubsMade(
	$apiPrivate,
	$apiPublic
);

// per fare una chiamata a questo punto, si chiama il metodo 
// con il nome uguale all'azione da chiamare unito all'entità
// per esempio, per chiamare l'azione 'get' dell'entità 'user'
// usiamo:

$cm->getUser(
	array(
		"id" => 19
	)
);
```
##### Di seguito le chiamate in php una volta inizializzato l'SDK

###### User

- Ricevere le informazioni di un'utente

```
$r = $cm->getUser(
	array(
		"id" => {id}
	)
);

echo json_encode($r->body);
```
Risposta:
```json
{
    "time": {
        "response": {
            "timestamp": 1412273978.6405,
            "formatted": "Thu, 02 Oct 2014 19:19:38 +0100"
        }
    },
    "code": 200,
    "result": {
        "user_id": "19",
        "ugid": "9",
        "username": "johnny",
        "contact": "2",
        "activation_code": "0c690fa4d6410bbb01c211e6274877fd",
        "reset_count": "0",
        "last_visit_date": "2014-10-02 19:18:20",
        "registration_date": "2014-03-10 17:52:00",
        "user_active_id": "10",
        "dt_last_modified": "2014-10-02 14:18:20",
        "operator_last_modifier": null,
        "radius": "60000000",
        "parent": "96",
        "user_group_id": "9",
        "timezone_offset": "2",
        "contact_id": "2",
        "firstname": "User",
        "middlename": " ",
        "lastname": "Fake",
        "fullname": "Johnny Walker Club",
        "prefix": " ",
        "suffix": " ",
        "nickname": " ",
        "firstphonetic": " ",
        "middlephonetic": " ",
        "lastphonetic": " ",
        "company": " ",
        "jobtitle": " ",
        "department": " ",
        "note": "NOTES",
        "birthday": null,
        "created": null,
        "modified": "2014-03-10 17:52:00",
        "kind": null,
        "iban": "ITXXXXXXXXXXXXXXXXXXXXXX",
        "email_id": "4",
        "calias": null,
        "email": "enisinanaj@outlook.com",
        "media_id": null,
        "type": null,
        "resizedfile": null,
        "originalfile": null,
        "phone_id": "1",
        "phone": "3394004112",
        "location_id": "40",
        "lat": "29.789",
        "long": "-80.2264",
        "address": "Jacopo da Ponte 62, Bassano del Grappa 36061"
    },
    "status": "ok",
    "error": null
}
```

- Autenticare un'utente:

```
$r = $cm->authenticateUser(
	array(
		"username" => {username},
		"password" => {password}
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412273900.6237,
            "formatted": "Thu, 02 Oct 2014 19:18:20 +0100"
        }
    },
    "code": 200,
    "result": "19",
    "status": "ok",
    "error": null
}
```

- Registrare un nuovo utente:

```
$r = $cm->putUser(
	array(
		"firstname"  => {firstname}, 		// optional
		"lastname"   => {lastname},			// optional
		"fullname"   => {fullname},			// optional
		"company"    => {company},			// optional
		"department" => {department},		// optional
		"note"       => {note},				// optional
		"email"      => {email},			
		"phone"      => {phone},			// suggested
		"username"   => {username},
		"password"   => {password},
		"usergroup"  => 1,					// 1
		"ccob"       => null				// optional
	)
);

echo json_encode($r->body);
```
Risposta:
```json
{
    "time": {
        "response": {
            "timestamp": 1412274552.8539,
            "formatted": "Thu, 02 Oct 2014 19:29:12 +0100"
        }
    },
    "code": 200,
    "result": 97,
    "status": "ok",
    "error": null
}
```

###### Club

- Prendere i dati di un club con il suo ID:

```
$r = $cm->getClub(
	array(
		"id" => {id}
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412280055.413,
            "formatted": "Thu, 02 Oct 2014 21:00:55 +0100"
        }
    },
    "code": 200,
    "result": {
        "user_id": "73",
        "ugid": "3",
        "username": "ilmuretto@clubsmade.com",
        "contact": "65",
        "activation_code": null,
        "reset_count": "0",
        "last_visit_date": "2014-09-21 10:27:18",
        "registration_date": "2014-07-18 13:29:05",
        "user_active_id": "10",
        "dt_last_modified": "2014-09-21 10:27:18",
        "operator_last_modifier": null,
        "radius": "60",
        "parent": "-1",
        "user_group_id": "3",
        "timezone_offset": "2",
        "contact_id": "65",
        "firstname": " ",
        "middlename": " ",
        "lastname": " ",
        "fullname": "Il Muretto",
        "prefix": " ",
        "suffix": " ",
        "nickname": " ",
        "firstphonetic": " ",
        "middlephonetic": " ",
        "lastphonetic": " ",
        "company": "Il Muretto",
        "jobtitle": " ",
        "department": "Clubbing",
        "note": "Periodo di apertura: Da APRILE a SETTEMBRE.\nDescrizione\nInfoLine:\nTITO PINTON  39 393 410 1120\nMARCO PIU  39 393 935 3880",
        "birthday": null,
        "created": null,
        "modified": "2014-07-18 13:29:05",
        "kind": null,
        "iban": "",
        "email_id": "68",
        "calias": null,
        "email": "ilmuretto@clubsmade.com",
        "media_id": "320",
        "type": "1",
        "resizedfile": "resized/b344c42739987fbfca0033361669660e.png",
        "originalfile": "original/b344c42739987fbfca0033361669660e.png",
        "phone_id": "49",
        "phone": null,
        "location_id": "41",
        "lat": "45.494",
        "long": "12.5902",
        "address": "Via Roma Destra, 120, Jesolo Venezia"
    },
    "status": "ok",
    "error": null
}
```

- Prendere i data delle discoteche entro una certa area:

```
$r = $cm->getClub(
	array(
		"lat" 		=> {latitude},		
		"long" 		=> {longitude},		
		"radius" 	=> {radius},		// optional - default: 100
		"offset" 	=> {offset},		// optional
		"limit"  	=> {limit}			// optional (required if offset is set)
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412281271.0756,
            "formatted": "Thu, 02 Oct 2014 21:21:11 +0100"
        }
    },
    "code": 200,
    "result": {
        "list": [
            {
                "user": "73",
                "contact": "65",
                "user_group_id": "3",
                "user_active_id": "10",
                "location": "41",
                "latitude": "45.494",
                "longitude": "12.5902",
                "address": "Via Roma Destra, 120, Jesolo Venezia",
                "fullname": "Il Muretto",
                "company": "Il Muretto",
                "note": "Periodo di apertura: Da APRILE a SETTEMBRE.\nDescrizione\nInfoLine:\nTITO PINTON  39 393 410 1120\nMARCO PIU  39 393 935 3880",
                "profile_media": "resized/b344c42739987fbfca0033361669660e.png",
                "distance": 98.3
            }
        ],
        "input": {
            "coords": {
                "lat": {
                    "min": 43.710678394081,
                    "max": 45.509321605919
                },
                "long": {
                    "min": 11.326936617815,
                    "max": 13.853463382185
                }
            },
            "latitude": "44.61",
            "longitude": "12.5902",
            "radius": "100"
        }
    },
    "status": "ok",
    "error": null
}
```

- Avere le cover di un `club`:

```
$r = $cm->coversClub(
	array(
		"club" 		=> {club},		
		"offset" 	=> {offset},		// optional
		"limit"  	=> {limit}			// optional (required if offset is set)
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412373636.1269,
            "formatted": "Fri, 03 Oct 2014 23:00:36 +0100"
        }
    },
    "code": 200,
    "result": [
        {
            "user_media_id": "39",
            "user": "41",
            "media": "111",
            "attribute": "profile",
            "id": "111",
            "filename": "605ff5590610093817ca0cc99d70cb47",
            "filesize": "129.4951171875 KB",
            "type": "1",
            "resizedfile": "resized/605ff5590610093817ca0cc99d70cb47.png",
            "originalfile": "original/605ff5590610093817ca0cc99d70cb47.png",
            "origin": "1",
            "timestamp": "2014-04-08 07:59:34",
            "exif": "a:0:{}"
        }
    ],
    "status": "ok",
    "error": null
}
```

- Avere le photo di un `club`:

```
$r = $cm->photosClub(
	array(
		"club" 		=> {club},		
		"offset" 	=> {offset},		// optional
		"limit"  	=> {limit}			// optional (required if offset is set)
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412373727.1821,
            "formatted": "Fri, 03 Oct 2014 23:02:07 +0100"
        }
    },
    "code": 200,
    "result": [
        {
            "user_media_id": "47",
            "user": "19",
            "media": "127",
            "attribute": "photo",
            "id": "127",
            "filename": "933459d29e1f7f3337b7b2db3f1723a9",
            "filesize": "24.7890625 KB",
            "type": "1",
            "resizedfile": "resized/933459d29e1f7f3337b7b2db3f1723a9.jpeg",
            "originalfile": "original/933459d29e1f7f3337b7b2db3f1723a9.jpeg",
            "origin": "1",
            "timestamp": "2014-04-10 09:26:41",
            "exif": "a:16:{s:15:\"exif:ColorSpace\";s:1:\"1\";s:28:\"exif:ComponentsConfiguration\";s:10:\"1, 2, 3, 0\";s:16:\"exif:Compression\";s:1:\"6\";s:20:\"exif:ExifImageLength\";s:4:\"2048\";s:19:\"exif:ExifImageWidth\";s:4:\"2048\";s:15:\"exif:ExifOffset\";s:3:\"102\";s:16:\"exif:ExifVersion\";s:14:\"48, 50, 50, 49\";s:20:\"exif:FlashPixVersion\";s:14:\"48, 49, 48, 48\";s:26:\"exif:JPEGInterchangeFormat\";s:3:\"286\";s:32:\"exif:JPEGInterchangeFormatLength\";s:4:\"8617\";s:16:\"exif:Orientation\";s:1:\"1\";s:19:\"exif:ResolutionUnit\";s:1:\"2\";s:21:\"exif:SceneCaptureType\";s:1:\"0\";s:16:\"exif:XResolution\";s:4:\"72/1\";s:21:\"exif:YCbCrPositioning\";s:1:\"1\";s:16:\"exif:YResolution\";s:4:\"72/1\";}"
        },
        {
            "user_media_id": "48",
            "user": "19",
            "media": "128",
            "attribute": "photo",
            "id": "128",
            "filename": "ac09b72b11c9c6dd0880b5ba4125de74",
            "filesize": "33.236328125 KB",
            "type": "1",
            "resizedfile": "resized/ac09b72b11c9c6dd0880b5ba4125de74.jpeg",
            "originalfile": "original/ac09b72b11c9c6dd0880b5ba4125de74.jpeg",
            "origin": "1",
            "timestamp": "2014-04-10 09:26:53",
            "exif": "a:16:{s:15:\"exif:ColorSpace\";s:1:\"1\";s:28:\"exif:ComponentsConfiguration\";s:10:\"1, 2, 3, 0\";s:16:\"exif:Compression\";s:1:\"6\";s:20:\"exif:ExifImageLength\";s:4:\"2048\";s:19:\"exif:ExifImageWidth\";s:4:\"2048\";s:15:\"exif:ExifOffset\";s:3:\"102\";s:16:\"exif:ExifVersion\";s:14:\"48, 50, 50, 49\";s:20:\"exif:FlashPixVersion\";s:14:\"48, 49, 48, 48\";s:26:\"exif:JPEGInterchangeFormat\";s:3:\"286\";s:32:\"exif:JPEGInterchangeFormatLength\";s:5:\"10356\";s:16:\"exif:Orientation\";s:1:\"1\";s:19:\"exif:ResolutionUnit\";s:1:\"2\";s:21:\"exif:SceneCaptureType\";s:1:\"0\";s:16:\"exif:XResolution\";s:4:\"72/1\";s:21:\"exif:YCbCrPositioning\";s:1:\"1\";s:16:\"exif:YResolution\";s:4:\"72/1\";}"
        }
    ],
    "status": "ok",
    "error": null
}
```

###### Event

- Prendere i dati di un'evento avendo il suo ID:

```
$r = $cm->getEvent(
	array(
		"id" => {id}
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412374582.1154,
            "formatted": "Fri, 03 Oct 2014 23:16:22 +0100"
        }
    },
    "code": 200,
    "result": {
        "event_id": "138",
        "name": "AD MAIORA NIGHT",
        "participants": "0",
        "created": "2014-09-25 15:57:57",
        "start": "2014-10-01 22:56:00",
        "end": "2014-10-04 07:56:00",
        "creator": "96",
        "d_id": "2",
        "t_id": "4",
        "theme_token": "ELECTRONIC",
        "club_name": "AD MAIORA",
        "dresscode_token": "BIKINI",
        "fb_id": "1475897666026981",
        "fb_al_id": "748093445225972",
        "description": "High night before death."
    },
    "status": "ok",
    "error": null
}
```

- Prendere i dati di tutti gli eventi di un club fornendo l'id del club:

```
$r = $cm->getEvent(
	array(
		"club" => {club}
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412374668.3386,
            "formatted": "Fri, 03 Oct 2014 23:17:48 +0100"
        }
    },
    "code": 200,
    "result": {
        "event_list": [
            {
                "event_id": "138",
                "name": "AD MAIORA NIGHT",
                "participants": "0",
                "created": "2014-09-25 15:57:57",
                "start": "2014-10-01 22:56:00",
                "end": "2014-10-04 07:56:00",
                "creator": "96",
                "d_id": "2",
                "t_id": "4",
                "theme_token": "ELECTRONIC",
                "description": "High night before death.",
                "dresscode_token": "BIKINI",
                "fb_id": "1475897666026981",
                "fb_al_id": "748093445225972"
            }
        ],
        "cover": [
            {
                "event_media_id": "220",
                "event": "138",
                "media": "361",
                "attribute": "cover",
                "fb_pub": "0",
                "id": "361",
                "filename": "ce5f4f70cc508f47284b003df9cfaf34",
                "filesize": "18.7958984375 KB",
                "type": "1",
                "resizedfile": "resized/ce5f4f70cc508f47284b003df9cfaf34.jpg",
                "originalfile": "original/ce5f4f70cc508f47284b003df9cfaf34.jpg",
                "origin": "1",
                "timestamp": "2014-09-25 15:57:58",
                "exif": "a:0:{}"
            }
        ]
    },
    "status": "ok",
    "error": null
}
```
- Prendere i dati di tutti gli eventi entro una certa area:

```
$r = $cm->getEvent(
	array(
		"lat" 		=> {latitude},		
		"long" 		=> {longitude},		
		"radius" 	=> {radius},		// optional - default: 100
		"offset" 	=> {offset},		// optional
		"limit"  	=> {limit}			// optional (required if offset is set)
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412376248.103,
            "formatted": "Fri, 03 Oct 2014 23:44:08 +0100"
        }
    },
    "code": 200,
    "result": {
        "list": [
            {
                "event_id": "143",
                "name": "angelina ce la dàà",
                "participants": "0",
                "event_notes": "jhgjhk",
                "start": "2014-09-28 18:50:00",
                "club_name": "Amnesia",
                "fb_id": null,
                "fb_al_id": null,
                "dresscode_id": "1",
                "theme_id": "4",
                "user": "103",
                "contact": "93",
                "user_group_id": "3",
                "user_active_id": "10",
                "location": "46",
                "latitude": "45.67",
                "longitude": "11.9",
                "address": "Via Roma Castelfranco veneto",
                "dresscode_token": "SUIT & TIE",
                "theme_token": "ELECTRONIC",
                "fullname": "Amnesia",
                "company": "Amnesia",
                "note": "THE CLUB\r\n\r\nThe club is located in Milano, Via Gatto, inside the parking ATM Forlanini. ",
                "profile_media": "resized/ad8276e25bda977c4f7903b3bc65f1eb.jpg",
                "event_cover_id": "387",
                "event_cover": "resized/0fe4b82ff90934aba43e5c065b0a6480.jpg",
                "event_cover_original": "original/0fe4b82ff90934aba43e5c065b0a6480.jpg",
                "min_price": "18.50",
                "distance": 16.56
            }
        ],
        "input": {
            "coords": {
                "lat": {
                    "min": 45.310339197041,
                    "max": 46.209660802959
                },
                "long": {
                    "min": 11.085477879486,
                    "max": 12.374522120514
                }
            },
            "latitude": "45.76",
            "longitude": "11.73",
            "radius": "50"
        }
    },
    "status": "ok",
    "error": null
}
```

- Prendere le cover di un'evento:

```
$r = $cm->coversEvent(
	array(
		"event" 	=> {event},
		"offset" 	=> {offset},		// optional
		"limit"  	=> {limit}			// optional (required if offset is set)
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412376433.3332,
            "formatted": "Fri, 03 Oct 2014 23:47:13 +0100"
        }
    },
    "code": 200,
    "result": [
        {
            "event_media_id": "225",
            "event": "143",
            "media": "387",
            "attribute": "cover",
            "fb_pub": "0",
            "id": "387",
            "filename": "0fe4b82ff90934aba43e5c065b0a6480",
            "filesize": "15.61328125 KB",
            "type": "1",
            "resizedfile": "http://demo.clubsmade.com/media/resized/0fe4b82ff90934aba43e5c065b0a6480.jpg",
            "originalfile": "http://demo.clubsmade.com/media/original/0fe4b82ff90934aba43e5c065b0a6480.jpg",
            "origin": "1",
            "timestamp": "2014-09-28 16:57:04",
            "exif": "a:0:{}"
        }
    ],
    "status": "ok",
    "error": null
}
```

- Prendere le photo di un'evento:

```
$r = $cm->photosEvent(
	array(
		"event" 	=> {event},
		"offset" 	=> {offset},		// optional
		"limit"  	=> {limit}			// optional (required if offset is set)
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412376531.3938,
            "formatted": "Fri, 03 Oct 2014 23:48:51 +0100"
        }
    },
    "code": 200,
    "result": [
        {
            "event_media_id": "227",
            "event": "142",
            "media": "389",
            "attribute": "photo",
            "fb_pub": "0",
            "id": "389",
            "filename": "6d4939eb6280e2a1441071980510fc6a",
            "filesize": "137.08203125 KB",
            "type": "1",
            "resizedfile": "http://demo.clubsmade.com/media/resized/6d4939eb6280e2a1441071980510fc6a.png",
            "originalfile": "http://demo.clubsmade.com/media/original/6d4939eb6280e2a1441071980510fc6a.png",
            "origin": "1",
            "timestamp": "2014-09-28 17:01:03",
            "exif": "a:0:{}"
        },
        {
            "event_media_id": "228",
            "event": "142",
            "media": "390",
            "attribute": "photo",
            "fb_pub": "0",
            "id": "390",
            "filename": "0f7fdcc111beb1ac3e5e0aa67b876dc4",
            "filesize": "768.6328125 KB",
            "type": "1",
            "resizedfile": "http://demo.clubsmade.com/media/resized/0f7fdcc111beb1ac3e5e0aa67b876dc4.png",
            "originalfile": "http://demo.clubsmade.com/media/original/0f7fdcc111beb1ac3e5e0aa67b876dc4.png",
            "origin": "1",
            "timestamp": "2014-09-28 17:01:22",
            "exif": "a:0:{}"
        }
    ],
    "status": "ok",
    "error": null
}
```

- Avere la lista dei contatti di un'evento:

```
$r = $cm->contactsEvent(
	array(
		"event" 	=> {event}
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412376662.895,
            "formatted": "Fri, 03 Oct 2014 23:51:02 +0100"
        }
    },
    "code": 200,
    "result": [
        {
            "user_id": "108",
            "ugid": "9",
            "username": "marcozona@clubsmade.com",
            "contact": "99",
            "activation_code": null,
            "reset_count": "0",
            "last_visit_date": "2014-10-01 16:56:13",
            "registration_date": "2014-09-28 15:50:43",
            "user_active_id": "10",
            "dt_last_modified": "2014-10-01 11:56:13",
            "operator_last_modifier": null,
            "radius": "60",
            "parent": "106",
            "user_group_id": "9",
            "timezone_offset": "2",
            "contact_id": "99",
            "firstname": "Marco",
            "middlename": " ",
            "lastname": "Zonta",
            "fullname": "Marco Zona",
            "prefix": " ",
            "suffix": " ",
            "nickname": " ",
            "firstphonetic": " ",
            "middlephonetic": " ",
            "lastphonetic": " ",
            "company": " ",
            "jobtitle": " ",
            "department": "Clubbing",
            "note": " ",
            "birthday": null,
            "created": null,
            "modified": "2014-09-28 10:50:43",
            "kind": null,
            "iban": "",
            "email_id": "102",
            "calias": null,
            "email": "marcozona@clubsmade.com",
            "media_id": "375",
            "type": "1",
            "resizedfile": "resized/5b0dc87c8b2d2953a984733dad3457d9.jpg",
            "originalfile": "original/5b0dc87c8b2d2953a984733dad3457d9.jpg",
            "phone_id": "83",
            "phone": "00000000000",
            "location_id": null,
            "lat": null,
            "long": null,
            "address": null
        }
    ],
    "status": "ok",
    "error": null
}
```

###### Ticket

- Prendere i dati dei ticket di un'evento:

```
$r = $cm->getTicket(
	array(
		"event" => {event}
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412376786.6709,
            "formatted": "Fri, 03 Oct 2014 23:53:06 +0100"
        }
    },
    "code": 200,
    "result": [
        {
            "ticket_id": "345",
            "description": "Ticket ingresso donna base",
            "price": "10.00",
            "dt_added": "2014-09-28 16:21:22",
            "discounted": "0",
            "discount_percentage": "0",
            "entry_ticket": "1",
            "user_id": "106",
            "event_id": "142",
            "dt_last_modified": "0000-00-00 00:00:00",
            "name": "Ingresso donna",
            "discount": "0",
            "max_sells": "50",
            "cur_avail": "0",
            "is_root": "0",
            "freedrinks": "1",
            "templateref": "337",
            "offline": "0",
            "sale_opts": "0"
        },
        {
            "ticket_id": "346",
            "description": "Ticket ingresso uomo base",
            "price": "15.00",
            "dt_added": "2014-09-28 16:21:22",
            "discounted": "0",
            "discount_percentage": "0",
            "entry_ticket": "1",
            "user_id": "106",
            "event_id": "142",
            "dt_last_modified": "0000-00-00 00:00:00",
            "name": "Ingresso uomo",
            "discount": "0",
            "max_sells": "50",
            "cur_avail": "0",
            "is_root": "0",
            "freedrinks": "1",
            "templateref": "336",
            "offline": "0",
            "sale_opts": "0"
        }
    ],
    "status": "ok",
    "error": null
}
```

- Prendere i dati di un ticket avendo il suo ID:

```
$r = $cm->getTicket(
	array(
		"event" => {event}
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412377176.0195,
            "formatted": "Fri, 03 Oct 2014 23:59:36 +0100"
        }
    },
    "code": 200,
    "result": {
        "event_id": "142",
        "name": "Ingresso donna",
        "participants": "76",
        "dt_added": "2014-09-28 16:21:22",
        "event_date": "2014-10-03 21:30:00",
        "creator": "106",
        "dresscode": "1",
        "theme": "2",
        "dt_last_modified": "0000-00-00 00:00:00",
        "end_date": "2014-10-04 02:00:00",
        "club_name": "Le Roi Club 1951",
        "description": "Ticket ingresso donna base",
        "fb_id": null,
        "fb_al_id": null,
        "fb_attending": null,
        "cancelled": "0",
        "id": "106",
        "username": "leroiclub1951@clubsmade.com",
        "ticket_id": "345",
        "price": "10.00",
        "discounted": "0",
        "discount_percentage": "0",
        "entry_ticket": "1",
        "user_id": "106",
        "discount": "0",
        "max_sells": "50",
        "cur_avail": "0",
        "is_root": "0",
        "freedrinks": "1",
        "templateref": "337",
        "offline": "0",
        "sale_opts": "0"
    },
    "status": "ok",
    "error": null
}
```

- Prenotare uno o più ticket per un'utente:

```
$r = $cm->bookTicket(
	array(
		"userid" => {userid},
		"ticket" => {ticket[,ticket[,ticket[,...]]]}
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412378035.9384,
            "formatted": "Sat, 04 Oct 2014 00:13:55 +0100"
        }
    },
    "code": 200,
    "result": 125,
    "status": "ok",
    "error": null
}
```

- Avere i dati di un'ordine tramite il suo ID:

```
$r = $cm->orderTicket(
	array(
		"id" => {order} 
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412378465.0219,
            "formatted": "Sat, 04 Oct 2014 00:21:05 +0100"
        }
    },
    "code": 200,
    "result": {
        "order_id": "1",
        "dt_added": "2013-01-01 00:00:00",
        "qr_code": "http://api.clubsmade.com/qr/get.json/msg/Order-73764ad71a0331a221e9f05b285e944b+1",
        "validated_qr": "0",
        "buyer": "19",
        "dt_last_modified": "2014-10-02 14:18:20",
        "checked_out": "0",
        "checked_out_time": "2014-04-21 06:00:00",
        "qr_activation_hash": " ",
        "id": "19",
        "username": "johnny",
        "contact": "2",
        "activation_code": "0c690fa4d6410bbb01c211e6274877fd",
        "operator_last_modifier": null,
        "location_id": "40",
        "radius": "60",
        "language": "eng",
        "timezone_offset": "2"
    },
    "status": "ok",
    "error": null
}
```

- Avere i dati degli ordini di un'utente:

```
$r = $cm->orderTicket(
	array(
		"user" 		=> {user},
		"bought" 	=> {bought} 		// optional, 0 o 1 
	)
);

echo json_encode($r->body);
```
Risposta:

```json
{
    "time": {
        "response": {
            "timestamp": 1412378703.0991,
            "formatted": "Sat, 04 Oct 2014 00:25:03 +0100"
        }
    },
    "code": 200,
    "result": [
        {
            "order_relation_id": "125",
            "ticket_id": "274",
            "order_id": "15",
            "ticket_qr_index": "81f9c5476c6a75356864f071dba79884",
            "final_price": "20",
            "ticket_qr_code": "/qr/get.json/msg/81f9c5476c6a75356864f071dba79884",
            "ticket_validated_qr": "0",
            "ticket_qr_activation_hash": null,
            "ticket_dt_qr_activation": null,
            "order_dt_added": "2014-10-04 00:13:55",
            "order_qr_code": null,
            "order_validated_qr": "0",
            "order_buyer": "19",
            "order_checked_out": "0",
            "order_checked_out_time": "0000-00-00 00:00:00",
            "order_activation_hash": " ",
            "ticket_description": "Entrata ridotta",
            "ticket_price": "0.20",
            "ticket_discounted": "0",
            "ticket_discount_percentage": "0",
            "entry_ticket": "1",
            "ticket_name": "ENRATA DONNA",
            "ticket_discount": "0",
            "ticket_freedrinks": "1",
            "event_id": "112",
            "event_name": "DAVIDE SQUILLACE",
            "event_start": "2014-07-20 00:00:00",
            "event_end": "2014-07-21 05:00:00",
            "event_club_name": "Il Muretto"
        },
        "http://api.clubsmade.com"
    ],
    "status": "ok",
    "error": null
}
```






















